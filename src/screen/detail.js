import React from 'react';
import {Image, Button, StyleSheet, Text, View } from 'react-native';


export default class DetailScreen extends React.Component {
  static navigationOptions = {
    title: 'Personal Detail',
  };

    render() {
      const { navigation } = this.props;
      const fn = navigation.getParam('firstname', 'Nobody');
      const ln = navigation.getParam('lastname', 'Someone');
      const img = navigation.getParam('photo', 'http://dummyimage.com/200x200.png/ff4444/ffffff');
      

      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text>Details Screen</Text>
          <Image source={{uri:img}} style={{width:300,height:300}}/>
          <Text style={{fontSize:20}}>{fn} {ln}</Text>
          <Button
            title="Go to Details... again"
            onPress={() => this.props.navigation.push('Details')}
          />
          <Button
            title="Go to Home"
            onPress={() => this.props.navigation.navigate('Home')}
          />
          <Button
            title="Go back"
            onPress={() => this.props.navigation.goBack()}
          />
        </View>
      );
    }
  }
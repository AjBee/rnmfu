import React from 'react';
import { SafeAreaView, TouchableOpacity, FlatList, StyleSheet, Text, View, AsyncStorage } from 'react-native';
import MyCard from '../components/mycard';

let uri = 'https://nuxtmfu.firebaseio.com/employees/.json';
let articleURI = 'https://nuxtmfu.firebaseio.com/articles/.json';

export default class HomeScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            personaldata: [],
            articlesdata: [],
            favid: 0,
            loading: false,
            error: null
        }
    }

    async fetchPersonalData() {
        this.setState({ loading: true });
        try {
            // let res = await fetch(uri, { method: 'GET' });
            let res = await fetch(articleURI, { method: 'GET' });
            let datas = await res.json();
            console.log("----- load data ------");
            console.log(datas);

            this.setState({
                // personaldata: datas,
                articlesdata: datas,
                loading: false,
            })
        } catch (error) {
            console.log(error);
            this.setState({
                // personaldata: [],
                articlesdata: [],
                loading: false,
            })
        }
    }

    async saveFav(id) {
        console.log("---->" + id);
        await AsyncStorage.setItem("favid", "" + id);
        this.setState({ "favid": id })
    }

    async loadFav() {
        let favid = await AsyncStorage.getItem("favid");
        this.setState({ "favid": favid })
        console.log("load---->" + favid);
    }

    async componentDidMount() {
        await this.loadFav();
        await this.fetchPersonalData();
    }

    render() {
        return (
            <SafeAreaView>
                <FlatList
                    data={this.state.articlesdata}
                    renderItem={({ item }) =>
                        <TouchableOpacity onPress={() =>
                            // this.props.navigation.navigate('Detail', item)
                            this.props.navigation.navigate('Article', item)
                            // this.saveFav(item.id)

                        }>
                            <MyCard items={item} fav={this.state.favid} />
                        </TouchableOpacity>


                    }
                />
            </SafeAreaView>
        );
    }
}



const personaldata = [{
    "id": 1,
    "firstname": "Bradley",
    "lastname": "Corselles",
    "email": "bcorselles0@yahoo.com",
    "phone": "185-691-7596",
    "photo": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQytVmTRp0e1uyezBqC7ohmTke2dil_S7UsK23M_axILbS0vx5t&s"
}, {
    "id": 2,
    "firstname": "Jeremias",
    "lastname": "Jacobbe",
    "email": "jjacobbe1@ucla.edu",
    "phone": "959-268-5001",
    "photo": "https://static.independent.co.uk/s3fs-public/thumbnails/image/2010/07/23/20/419247.bin?w968h681"
}, {
    "id": 3,
    "firstname": "Ofilia",
    "lastname": "Joel",
    "email": "ojoel2@webs.com",
    "phone": "502-850-3974",
    "photo": "https://pbs.twimg.com/profile_images/988775660163252226/XpgonN0X_400x400.jpg"
}, {
    "id": 4,
    "firstname": "Merwin",
    "lastname": "Zima",
    "email": "mzima3@sohu.com",
    "phone": "817-452-5248",
    "photo": "http://dummyimage.com/200x200.png/ff4444/ffffff"
}, {
    "id": 5,

    "lastname": "Huggill",
    "email": "ehuggill4@mozilla.com",
    "phone": "415-966-2962",

}, {
    "id": 6,
    "firstname": "Kennan",
    "lastname": "Meakin",
    "email": "kmeakin5@csmonitor.com",
    "phone": "305-142-2242",
    "photo": "http://dummyimage.com/200x200.png/cc0000/ffffff"
}, {
    "id": 7,
    "firstname": "Harland",
    "lastname": "Braden",
    "email": "hbraden6@rakuten.co.jp",
    "phone": "901-498-2625",
    "photo": "http://dummyimage.com/200x200.png/dddddd/000000"
}, {
    "id": 8,
    "firstname": "Jacynth",
    "lastname": "Greasty",
    "email": "jgreasty7@storify.com",
    "phone": "282-607-9069",
    "photo": "http://dummyimage.com/200x200.png/5fa2dd/ffffff"
}, {
    "id": 9,
    "firstname": "Hanni",
    "lastname": "Bavester",
    "email": "hbavester8@ovh.net",
    "phone": "456-606-6627",
    "photo": "http://dummyimage.com/200x200.png/dddddd/000000"
}, {
    "id": 10,
    "firstname": "Sampson",
    "lastname": "McAviy",
    "email": "smcaviy9@ft.com",
    "phone": "717-185-3178",
    "photo": "http://dummyimage.com/200x200.png/dddddd/000000"
}];

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

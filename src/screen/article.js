import React from 'react';
import { Image, TextInput,Button, StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview'
import { Dimensions } from "react-native";
import QRCode1 from 'react-native-qrcode-generator';

// import QRCode from 'react-native-qrcode-svg'

const w = Math.round(Dimensions.get('window').width);
const h = Math.round(Dimensions.get('window').height);

export default class ArticleScreen extends React.Component {
    static navigationOptions = {
        title: 'Read Article',
    };
    constructor(props){
        super(props);
        this.state={
            text:""
        }
    }
    render() {
        const { navigation } = this.props;
        const id = navigation.getParam('id', '0');
        const title = navigation.getParam('title', 'No Title');
        const img = navigation.getParam('img', 'http://dummyimage.com/200x200.png/ff4444/ffffff');
        const link = navigation.getParam('link', 'http://ajbee.me/2019/09/05/barcamp-chiangrai-2019-30-nov-2019/');


        return (
            <View style={{ flex: 1, }}>
                
                <QRCode1
                    value={link}
                    size={400}
                    />
           
                {/* <WebView
                    source={{ uri: link }}
                    style={{ height: h, width: w }}
                /> */}
            </View >
        );
    }
}
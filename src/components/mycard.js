import React from 'react';
import { Image, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const showFavIcon = (pid, favid) => {
    console.log(pid + ", " + favid)
    if(pid==favid){
        return (<Icon name="ios-home" size={40} />)
    }  
} 


const MyCard = (props) => {
    // const items = props.items;
    // let { id, firstname, lastname, email, phone, photo } = props.items;
    let favid = props.fav;
    let {id,img,link,title} = props.items;

    return (
        <View style={{
            flexDirection: 'row', padding: 5,
            borderBottomColor: 'gray', borderBottomWidth: 0.2
        }}>
            <Image source={{ uri: img }}
                style={{ borderRadius: 50, height: 80, width: 80 }} />
            <View style={{ flex: 1, paddingTop: 5, paddingLeft: 10 }}>
                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{title} {showFavIcon(id,favid)} </Text>
                {/* <Text style={{ fontSize: 14 }}>
                    <Icon name="ios-mail" size={30} color="#900" />: {email}</Text>
                <Text style={{ fontSize: 14 }}>
                test
                <Icon name="ios-phone-portrait" size={30} color="#900" />: {phone}</Text> */}
            </View>
        </View>
    );
}


export default MyCard;
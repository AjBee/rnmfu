import React from 'react';
import { SafeAreaView } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import Icon from 'react-native-vector-icons/Ionicons';

import HomeScreen from './src/screen/home';
import DetailScreen from './src/screen/detail';
import ArticleScreen from './src/screen/article';
import ScanScreen from './src/screen/scanner';

const StackAppNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen,
  },
  Article: {
    screen: ArticleScreen,
  }
},
  {
    initialRouteName: 'Home',
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }
);

const TabNavigator = createBottomTabNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      tabBarLabel: "บ้าน",
      tabBarIcon: <Icon name="ios-home" size={30} />
    }
  },
  Scan: ScanScreen,
  Detail: DetailScreen,
  Stack: StackAppNavigator,
});


const AppContainer = createAppContainer(TabNavigator);

export default class App extends React.Component {
  render() {
    return (
      
        <AppContainer />
    
      )
  }
}